#include "lwtnn/LightweightGraph.hh"
#include "lwtnn/parse_json.hh"

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <cmath>

#include <random>
#include <algorithm>

// For vertex inputs we have an outer map (of input nodes) and an
// inner map (of keyed values)
typedef std::map<std::string, std::map<std::string, double> > input_t;

// For tracks the inputs are similar, but each input node gets a map
// of vectors. The vectors must all be the same length.
typedef std::map<std::string, std::vector<double> > map_vec_t;
typedef std::map<std::string, map_vec_t> inputv_t;

// From StackOverflow
static std::vector<double> generate_data(size_t size)
{
    using value_type = double;
    // We use static in order to instantiate the random engine
    // and the distribution once only.
    // It may provoke some thread-safety issues.
    static std::uniform_real_distribution<value_type> distribution(-1.0, 1.0);
    static std::default_random_engine generator;

    std::vector<value_type> data(size);
    std::generate(data.begin(), data.end(), []() { return distribution(generator); });
    return data;
}

int main(int argc, char* argv[]) {
  if (argc <= 1) {
    puts("point me to the saved lwtnn network");
    exit(1);
  }

  int nFeatures = std::stoi(argv[2]);

  using namespace lwt;
  std::ifstream input(argv[1]);

  std::map<std::string, std::map<std::string, double> > inputData;

  // The graph object initializes from a GraphConfig object
  LightweightGraph graph(parse_json_graph(input));

  for (int i = 0; i < 10000; i++){

    for (int v = 0; v < nFeatures; v++) {
      // Generate one input data point per feature
      inputData["node_0"][std::string("variable_") + std::to_string(v)] = generate_data(1)[0];
    }

    auto flavmap = graph.compute(inputData);
  }

  return 0;
}
