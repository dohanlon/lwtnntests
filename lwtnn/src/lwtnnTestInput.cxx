#include <iostream>
#include <fstream>
#include <string>
#include <map>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/math/special_functions/relative_difference.hpp>

#include "lwtnn/LightweightGraph.hh"
#include "lwtnn/parse_json.hh"

typedef std::map<std::string, std::map<std::string, std::vector<double>> > sequences_t;

std::pair<sequences_t, double> parseInputs(std::ifstream & data)
{

  // For the input array

  std::map<std::string, std::map<std::string, std::vector<double>> > input_sequences;

  boost::property_tree::ptree pt;
  boost::property_tree::read_json(data, pt);

  int variableNumber = 0;
  BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pt.get_child("input")) {

    // Loop over columns

    std::vector<double> varVector;

    BOOST_FOREACH (boost::property_tree::ptree::value_type& itemPair, v.second) {

      // Loop over rows

      double val = itemPair.second.get_value<double>();
      varVector.push_back(val);
    }

    input_sequences["node_0"]["variable_" + std::to_string(variableNumber)] = varVector;
    variableNumber++;
  }

  // For the output array (of shape 1)

  double output = 0;

  BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pt.get_child("output")) {

    output = v.second.get_value<double>();
  }

  return std::make_pair(input_sequences, output);
}

int main(int argc, char* argv[]) {
  if (argc <= 1) {
    puts("point me to the saved lwtnn network");
    exit(1);
  }

  std::ifstream input(argv[1]);
  std::ifstream data(argv[2]);

  std::pair<sequences_t, double> inputOutputPair = parseInputs(data);
  sequences_t input_sequences = inputOutputPair.first;
  double output = inputOutputPair.second;

  lwt::LightweightGraph graph(lwt::parse_json_graph(input));

  auto flavmap = graph.compute({}, input_sequences);

  double testOutput = 0;
  for (const auto& flav: flavmap) {
    testOutput = flav.second;
  }

  std::cout << testOutput << std::endl;

  double difference = boost::math::relative_difference(output, testOutput);

  if (difference < 1E-6) return 0;
  else return 1;
}
