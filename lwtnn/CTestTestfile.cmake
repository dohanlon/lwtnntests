# CMake generated Testfile for 
# Source directory: /home/LHCB/daniel/lwtnn
# Build directory: /home/LHCB/daniel/lwtnn
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test-nn-streamers_ctest "/home/LHCB/daniel/lwtnn/test-bin/test-nn-streamers")
set_tests_properties(test-nn-streamers_ctest PROPERTIES  WORKING_DIRECTORY "/home/LHCB/daniel/lwtnn/test-bin")
