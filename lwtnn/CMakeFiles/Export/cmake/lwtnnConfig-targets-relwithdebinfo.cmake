#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "lwtnn::lwtnn" for configuration "RelWithDebInfo"
set_property(TARGET lwtnn::lwtnn APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(lwtnn::lwtnn PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/liblwtnn.so"
  IMPORTED_SONAME_RELWITHDEBINFO "liblwtnn.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS lwtnn::lwtnn )
list(APPEND _IMPORT_CHECK_FILES_FOR_lwtnn::lwtnn "${_IMPORT_PREFIX}/lib/liblwtnn.so" )

# Import target "lwtnn::lwtnn-stat" for configuration "RelWithDebInfo"
set_property(TARGET lwtnn::lwtnn-stat APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(lwtnn::lwtnn-stat PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "CXX"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/liblwtnn-stat.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS lwtnn::lwtnn-stat )
list(APPEND _IMPORT_CHECK_FILES_FOR_lwtnn::lwtnn-stat "${_IMPORT_PREFIX}/lib/liblwtnn-stat.a" )

# Import target "lwtnn::lwtnn-benchmark-rnn" for configuration "RelWithDebInfo"
set_property(TARGET lwtnn::lwtnn-benchmark-rnn APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(lwtnn::lwtnn-benchmark-rnn PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/lwtnn-benchmark-rnn"
  )

list(APPEND _IMPORT_CHECK_TARGETS lwtnn::lwtnn-benchmark-rnn )
list(APPEND _IMPORT_CHECK_FILES_FOR_lwtnn::lwtnn-benchmark-rnn "${_IMPORT_PREFIX}/bin/lwtnn-benchmark-rnn" )

# Import target "lwtnn::lwtnn-dump-config" for configuration "RelWithDebInfo"
set_property(TARGET lwtnn::lwtnn-dump-config APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(lwtnn::lwtnn-dump-config PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/lwtnn-dump-config"
  )

list(APPEND _IMPORT_CHECK_TARGETS lwtnn::lwtnn-dump-config )
list(APPEND _IMPORT_CHECK_FILES_FOR_lwtnn::lwtnn-dump-config "${_IMPORT_PREFIX}/bin/lwtnn-dump-config" )

# Import target "lwtnn::lwtnn-test-arbitrary-net" for configuration "RelWithDebInfo"
set_property(TARGET lwtnn::lwtnn-test-arbitrary-net APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(lwtnn::lwtnn-test-arbitrary-net PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/lwtnn-test-arbitrary-net"
  )

list(APPEND _IMPORT_CHECK_TARGETS lwtnn::lwtnn-test-arbitrary-net )
list(APPEND _IMPORT_CHECK_FILES_FOR_lwtnn::lwtnn-test-arbitrary-net "${_IMPORT_PREFIX}/bin/lwtnn-test-arbitrary-net" )

# Import target "lwtnn::lwtnn-test-graph" for configuration "RelWithDebInfo"
set_property(TARGET lwtnn::lwtnn-test-graph APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(lwtnn::lwtnn-test-graph PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/lwtnn-test-graph"
  )

list(APPEND _IMPORT_CHECK_TARGETS lwtnn::lwtnn-test-graph )
list(APPEND _IMPORT_CHECK_FILES_FOR_lwtnn::lwtnn-test-graph "${_IMPORT_PREFIX}/bin/lwtnn-test-graph" )

# Import target "lwtnn::lwtnn-test-lightweight-graph" for configuration "RelWithDebInfo"
set_property(TARGET lwtnn::lwtnn-test-lightweight-graph APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(lwtnn::lwtnn-test-lightweight-graph PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/lwtnn-test-lightweight-graph"
  )

list(APPEND _IMPORT_CHECK_TARGETS lwtnn::lwtnn-test-lightweight-graph )
list(APPEND _IMPORT_CHECK_FILES_FOR_lwtnn::lwtnn-test-lightweight-graph "${_IMPORT_PREFIX}/bin/lwtnn-test-lightweight-graph" )

# Import target "lwtnn::lwtnn-test-rnn" for configuration "RelWithDebInfo"
set_property(TARGET lwtnn::lwtnn-test-rnn APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(lwtnn::lwtnn-test-rnn PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/lwtnn-test-rnn"
  )

list(APPEND _IMPORT_CHECK_TARGETS lwtnn::lwtnn-test-rnn )
list(APPEND _IMPORT_CHECK_FILES_FOR_lwtnn::lwtnn-test-rnn "${_IMPORT_PREFIX}/bin/lwtnn-test-rnn" )

# Import target "lwtnn::lwtnnInterface" for configuration "RelWithDebInfo"
set_property(TARGET lwtnn::lwtnnInterface APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(lwtnn::lwtnnInterface PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/lwtnnInterface"
  )

list(APPEND _IMPORT_CHECK_TARGETS lwtnn::lwtnnInterface )
list(APPEND _IMPORT_CHECK_FILES_FOR_lwtnn::lwtnnInterface "${_IMPORT_PREFIX}/bin/lwtnnInterface" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
