# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.11

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.11.0/Linux-x86_64/bin/cmake

# The command to remove a file.
RM = /cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.11.0/Linux-x86_64/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/LHCB/daniel/lwtnn

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/LHCB/daniel/lwtnn

# Include any dependencies generated for this target.
include CMakeFiles/lwtnnInterface.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/lwtnnInterface.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/lwtnnInterface.dir/flags.make

CMakeFiles/lwtnnInterface.dir/src/lwtnnInterface.cxx.o: CMakeFiles/lwtnnInterface.dir/flags.make
CMakeFiles/lwtnnInterface.dir/src/lwtnnInterface.cxx.o: src/lwtnnInterface.cxx
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/LHCB/daniel/lwtnn/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/lwtnnInterface.dir/src/lwtnnInterface.cxx.o"
	/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_87/gcc/4.9.3/x86_64-slc6/bin/g++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/lwtnnInterface.dir/src/lwtnnInterface.cxx.o -c /home/LHCB/daniel/lwtnn/src/lwtnnInterface.cxx

CMakeFiles/lwtnnInterface.dir/src/lwtnnInterface.cxx.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/lwtnnInterface.dir/src/lwtnnInterface.cxx.i"
	/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_87/gcc/4.9.3/x86_64-slc6/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/LHCB/daniel/lwtnn/src/lwtnnInterface.cxx > CMakeFiles/lwtnnInterface.dir/src/lwtnnInterface.cxx.i

CMakeFiles/lwtnnInterface.dir/src/lwtnnInterface.cxx.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/lwtnnInterface.dir/src/lwtnnInterface.cxx.s"
	/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_87/gcc/4.9.3/x86_64-slc6/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/LHCB/daniel/lwtnn/src/lwtnnInterface.cxx -o CMakeFiles/lwtnnInterface.dir/src/lwtnnInterface.cxx.s

# Object files for target lwtnnInterface
lwtnnInterface_OBJECTS = \
"CMakeFiles/lwtnnInterface.dir/src/lwtnnInterface.cxx.o"

# External object files for target lwtnnInterface
lwtnnInterface_EXTERNAL_OBJECTS =

bin/lwtnnInterface: CMakeFiles/lwtnnInterface.dir/src/lwtnnInterface.cxx.o
bin/lwtnnInterface: CMakeFiles/lwtnnInterface.dir/build.make
bin/lwtnnInterface: lib/liblwtnn-stat.a
bin/lwtnnInterface: CMakeFiles/lwtnnInterface.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/LHCB/daniel/lwtnn/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable bin/lwtnnInterface"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/lwtnnInterface.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/lwtnnInterface.dir/build: bin/lwtnnInterface

.PHONY : CMakeFiles/lwtnnInterface.dir/build

CMakeFiles/lwtnnInterface.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/lwtnnInterface.dir/cmake_clean.cmake
.PHONY : CMakeFiles/lwtnnInterface.dir/clean

CMakeFiles/lwtnnInterface.dir/depend:
	cd /home/LHCB/daniel/lwtnn && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/LHCB/daniel/lwtnn /home/LHCB/daniel/lwtnn /home/LHCB/daniel/lwtnn /home/LHCB/daniel/lwtnn /home/LHCB/daniel/lwtnn/CMakeFiles/lwtnnInterface.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/lwtnnInterface.dir/depend

