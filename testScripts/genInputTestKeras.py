import numpy as np

from keras.models import Model
from keras.layers import *

import json

def makeModel(nTracks, nVars, nodes):
    input = Input((nTracks, nVars)) # Input((3, 2)) -> 3 tracks 2 vars

    tracks = Masking(mask_value=-999, name='track_masking')(input)

    # Incompatible with Dense w/o TD here

    tracks = TimeDistributed(Dense(nodes, activation = 'relu', kernel_initializer = 'glorot_uniform', name = 'dense0'))(tracks)
    tracks = Dropout(0.5)(tracks)

    tracks = GRU(nodes, kernel_initializer = 'glorot_uniform', return_sequences = True, name = 'trackGRU0')(tracks)
    tracks = GRU(nodes, kernel_initializer = 'glorot_uniform', return_sequences = False, name = 'trackGRU1')(tracks)
    tracks = Dropout(0.5)(tracks)

    tracks = Dense(nodes, activation = 'relu', kernel_initializer = 'glorot_uniform', name = 'dense1')(tracks)
    output = Dense(1, activation = 'sigmoid', name = 'output')(tracks)

    model = Model(inputs = input, outputs = output)

    return model

if __name__ == '__main__':

    import sys

    nodes = 32
    nTracks = int(sys.argv[1])
    nVars = int(sys.argv[2])

    # Generate a single entry batch of shape (nTracks, nVars)
    inputData = np.random.uniform(-1.0, 1.0, size = (1, nTracks, nVars))

    model = makeModel(nTracks, nVars, nodes)

    model.summary()

    arch = model.to_json()
    with open('lwtnnTestData/architecture_InputTest_' + str(nodes) + '_' + str(nTracks) + '_' + str(nVars) +  '.json', 'w') as arch_file:
        arch_file.write(arch)

    model.save_weights('lwtnnTestData/weights_InputTest_' + str(nodes) + '_' + str(nTracks) + '_' + str(nVars) +  '.h5')

    output = model.predict(inputData)

    # Remove the batch dimension, and make this variable wise (like lwtnn wants)
    inputData = inputData.reshape(nTracks, nVars).T.tolist()

    inputOutput = {'input' : inputData, 'output' : output.reshape(1).tolist()}

    json.dump(inputOutput, open('lwtnnTestData/inputTestData.json', 'w'))
