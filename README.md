LWTNN Tests
==========

Tests and benchmarks for using [lwtnn](https://github.com/lwtnn/lwtnn) (C++) for flavour tagging RNNs trained in Keras.

Requirements
-----
Eigen, Boost

Setup
-----

Example setup on CentOS 7 (or SLC 6 with appropriate s/centos7/slc6) can be found in the [gitlab-ci file](.gitlab-ci.yml).

LWTNN contains scripts to process Keras model (.json) and weight (.h5) files, into input configuration .json files (see [here](benchmarkScripts/lwtnnTests.py#L20) for an example) using Python 3.

For more information on these see the [LWTNN documentation](https://github.com/lwtnn/lwtnn/wiki/Examples).

Usage
-----

Scripts in [benchmarkScripts](benchmarkScripts) generate the files required to generate random network weights with different network configurations. These files are converted into the LWTNN .json format, and read by the [lwtnnInterface](lwtnn/src/lwtnnInterface.cxx). This attempts to emulate calling the network with various (random) inputs, and the run time and memory usage is recorded for each network configuration.

Tests that ensure that the results obtained from running these networks in Keras and with the LWTNN C++ interface are the same, to reasonable precision, are located in [testScripts](testScripts) and are executed using [lwtnnTestInput](lwtnn/src/lwtnnTestInput.cxx).

The contents of [.gitlab-ci.yml](.gitlab-ci.yml) is probably also useful information on how to set up the Python3/Keras/LWTNN stack on an arbitrary machine.

Results
-------

Some preliminary results of the timing tests can be found in the slides [here](https://indico.cern.ch/event/807893/contributions/3362729/attachments/1818020/2972246/20190327-IFT-FT.pdf).
