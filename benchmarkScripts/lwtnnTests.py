import subprocess as sp
import re

from pprint import pprint

import pickle

from tqdm import tqdm

nodesList = [8, 16, 32, 64, 128]
nTracksList = [10, 20, 30, 50, 75, 100]
varsList = [5, 10, 15, 20, 25, 50]

nCategoriesList = [2, 4, 8, 16, 32]

def generateTagJSON():

    for nodes in nodesList:
        for nTracks in nTracksList:
            for vars in varsList:

                cmd1 = '~/anaconda3/bin/python ../lwtnn/converters/kerasfunc2json.py architecture_test_' + str(nodes) + '_' + str(nTracks) + '_' + str(vars) +  '_tag.json weights_test_' + str(nodes) + '_' + str(nTracks) + '_' + str(vars) +  '_tag.h5 > variables_test_' + str(nodes) + '_' + str(nTracks) + '_' + str(vars) +  '_tag.json'
                cmd2 = '~/anaconda3/bin/python ../lwtnn/converters/kerasfunc2json.py architecture_test_' + str(nodes) + '_' + str(nTracks) + '_' + str(vars) +  '_tag.json weights_test_' + str(nodes) + '_' + str(nTracks) + '_' + str(vars) +  '_tag.h5 variables_test_' + str(nodes) + '_' + str(nTracks) + '_' + str(vars) +  '_tag.json > nn_test_' + str(nodes) + '_' + str(nTracks) + '_' + str(vars) +  '_tag.json'

                sp.call(cmd1, shell = True)
                sp.call(cmd2, shell = True)

def generateCatJSON():

    for nodes in nodesList:
        for vars in varsList:
            for nCat in nCategoriesList:

                cmd1 = '~/anaconda3/bin/python ../lwtnn/converters/kerasfunc2json.py architecture_test_' + str(nodes) + '_' + str(vars) + '_' + str(nCat) +  '_cat.json weights_test_' + str(nodes) + '_' + str(vars) + '_' + str(nCat) +  '_cat.h5 > variables_test_' + str(nodes) + '_' + str(vars) + '_' + str(nCat) +  '_cat.json'
                cmd2 = '~/anaconda3/bin/python ../lwtnn/converters/kerasfunc2json.py architecture_test_' + str(nodes) + '_' + str(vars) + '_' + str(nCat) +  '_cat.json weights_test_' + str(nodes) + '_' + str(vars) + '_' + str(nCat) +  '_cat.h5 variables_test_' + str(nodes) + '_' + str(vars) + '_' + str(nCat) +  '_cat.json > nn_test_' + str(nodes) + '_' + str(vars) + '_' + str(nCat) +  '_cat.json'

                sp.call(cmd1, shell = True)
                sp.call(cmd2, shell = True)

def parseTimeInfo(ret):

    time = float(re.search('User time \(seconds\): (.+)', ret).group(1))
    memory = float(re.search('Maximum resident set size \(kbytes\): (.+)', ret).group(1))

    return time, memory

def benchmark(command):

    ret = sp.check_output('/usr/bin/time -v ' + command, shell = True, stderr = sp.STDOUT)

    return parseTimeInfo(ret)

def runTagBenchmark():

    timing = {}
    memoryUsage = {}

    for nodes in tqdm(nodesList):
        for nTracks in nTracksList:
            for vars in varsList:

                time, memory = benchmark('lwtnn/bin/lwtnnInterface /home/LHCB/daniel/lwtnnTests/nn_test_' + str(nodes) + '_' + str(nTracks) + '_' + str(vars) + '_tag.json ' + str(vars) + ' ' + str(nTracks))
                timing[str(nodes) + '-' + str(nTracks) + '-' + str(vars)] = time / 10000.
                memoryUsage[str(nodes) + '-' + str(nTracks) + '-' + str(vars)] = memory / 1024.

                print(nodes, nTracks, vars, time, memory)

    pprint(timing)
    pprint(memoryUsage)

    pickle.dump(timing, open('lwtnnTimingTag.pkl', 'w'))
    pickle.dump(memoryUsage, open('lwtnnMemoryUsageTag.pkl', 'w'))

def runCatBenchmark():

    timing = {}
    memoryUsage = {}

    for nodes in tqdm(nodesList):
        for vars in varsList:
            for nCat in nCategoriesList:

                time, memory = benchmark('lwtnn/bin/lwtnnInterfaceFlat /home/LHCB/daniel/lwtnnTestsCat/nn_test_' + str(nodes) + '_' + str(vars) + '_' + str(nCat) + '_cat.json ' + str(vars))

                timing[str(nodes) + '-' + str(vars) + '-' + str(nCat)] = time / 10000.
                memoryUsage[str(nodes) + '-' + str(vars) + '-' + str(nCat)] = memory / 1024.

                print(nodes, vars, nCat, time, memory)

    pprint(timing)
    pprint(memoryUsage)

    pickle.dump(timing, open('lwtnnTimingCat.pkl', 'w'))
    pickle.dump(memoryUsage, open('lwtnnMemoryUsageCat.pkl', 'w'))

if __name__ == '__main__':

    # print(benchmark('lwtnn/bin/lwtnnInterface /home/LHCB/daniel/lwtnnTests/nn_test_8_20_5.json 20 5'))
    # generateCatJSON()
    runCatBenchmark()
