import keras
from keras.models import Model

def tagModel(nodes, nTracks, vars):

    '''
    Recurrent network for calculating flavour tag.
    '''

    input = keras.layers.Input((nTracks, vars))

    tracks = keras.layers.Masking(mask_value=-999, name='track_masking')(input)

    # Incompatible with Dense w/o TD here

    tracks = keras.layers.TimeDistributed(Dense(nodes, activation = 'relu', kernel_initializer = 'glorot_uniform', name = 'dense0'))(tracks)
    tracks = keras.layers.Dropout(0.5)(tracks)

    tracks = keras.layers.GRU(nodes, kernel_initializer = 'glorot_uniform', return_sequences = True, name = 'trackGRU0')(tracks)
    tracks = keras.layers.GRU(nodes, kernel_initializer = 'glorot_uniform', return_sequences = False, name = 'trackGRU1')(tracks)
    tracks = keras.layers.Dropout(0.5)(tracks)

    tracks = keras.layers.Dense(nodes, activation = 'relu', kernel_initializer = 'glorot_uniform', name = 'dense1')(tracks)
    output = keras.layers.Dense(1, activation = 'sigmoid', name = 'output')(tracks)

    return Model(inputs = input, outputs = output)

def catModel(nodes, vars, trackCategories):

    '''
    Feed-forward network for calculating track category (out of 'trackCategories' categories).
    '''

    input = keras.layers.Input((vars,))

    tracks = keras.layers.Dense(nodes, activation = 'relu', name = 'cat_dense1')(input)
    tracks = keras.layers.Dense(nodes, activation = 'relu', name = 'cat_dense2')(tracks)

    tracks = keras.layers.BatchNormalization(momentum = 0.99)(tracks)
    tracks = keras.layers.Dense(nodes, activation = 'relu', name = 'cat_dense3')(tracks)
    tracks = keras.layers.Dense(nodes, activation = 'relu', name = 'cat_dense4')(tracks)

    tracks = keras.layers.BatchNormalization(momentum = 0.99)(tracks)
    tracks = keras.layers.Dense(nodes, activation = 'relu', name = 'cat_dense5')(tracks)
    tracks = keras.layers.Dense(nodes, activation = 'relu', name = 'cat_dense6')(tracks)
    tracks = keras.layers.Dropout(0.5)(tracks)

    outputCat = keras.layers.Dense(trackCategories, activation = 'softmax', name = 'outputCat')(tracks)

    return Model(inputs = input, outputs = outputCat)

def generateTagBenchmarkFiles():

    nodesList = [8, 16, 32, 64, 128]
    nTracksList = [10, 20, 30, 50, 75, 100]
    varsList = [5, 10, 15, 20, 25, 50]

    for nodes in nodesList:
        for nTracks in nTracksList:
            for vars in varsList:

                model = tagModel(nodes, nTracks, vars)

                arch = model.to_json()
                with open('lwtnnTests/architecture_test_' + str(nodes) + '_' + str(nTracks) + '_' + str(vars) +  '_tag.json', 'w') as arch_file:
                    arch_file.write(arch)

                model.save_weights('lwtnnTests/weights_test_' + str(nodes) + '_' + str(nTracks) + '_' + str(vars) +  '_tag.h5')

def generateCatBenchmarkFiles():

    nodesList = [8, 16, 32, 64, 128]
    varsList = [5, 10, 15, 20, 25, 50]
    nCategoriesList = [2, 4, 8, 16, 32]

    for nodes in nodesList:
        for vars in varsList:
            for nCat in nCategoriesList:

                model = catModel(nodes, vars, nCat)

                arch = model.to_json()
                with open('lwtnnTests/architecture_test_' + str(nodes) + '_' + str(vars) + '_' + str(nCat) +  '_cat.json', 'w') as arch_file:
                    arch_file.write(arch)

                model.save_weights('lwtnnTests/weights_test_' + str(nodes) + '_' + str(vars) + '_' + str(nCat) +  '_cat.h5')

if __name__ == '__main__':
    generateCatBenchmarkFiles()
