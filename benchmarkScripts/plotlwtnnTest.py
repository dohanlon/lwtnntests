import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
# plt.style.use('ggplot')
plt.style.use(['fivethirtyeight', 'seaborn-whitegrid', 'seaborn-ticks'])
from matplotlib import rcParams
from matplotlib import gridspec
import matplotlib.ticker as plticker

from matplotlib import cm

from tqdm import tqdm

import pickle

import pandas as pd

rcParams['axes.facecolor'] = 'FFFFFF'
rcParams['savefig.facecolor'] = 'FFFFFF'
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'

rcParams.update({'figure.autolayout': True})

nodesList = [8, 16, 32, 64, 128]
nTracksList = [10, 20, 30, 50, 75, 100]
varsList = [5, 10, 15, 20, 25, 50]

nCategoriesList = [2, 4, 8, 16, 32]

def plotTagBenchmarks():

    time = pickle.load( open('lwtnnTiming.pkl', 'r') )
    memory = pickle.load( open('lwtnnMemoryUsage.pkl', 'r') )

    rows_list = []

    for nodes in nodesList:
        for nTracks in nTracksList:
            for vars in varsList:
                d = {'nodes' : nodes, 'tracks' : nTracks, 'vars' : vars, 'time' : time[str(nodes) + '-' + str(nTracks) + '-' + str(vars)] * 1E3, 'memory' : memory[str(nodes) + '-' + str(nTracks) + '-' + str(vars)]}
                rows_list.append(d)

    df = pd.DataFrame(rows_list)

    # nTracks for fixed nodes = 32, vars = 5, 20, 50
    nTracks20 = df.query('nodes == 32 & vars == 20').sort_values(by = ['tracks'])
    nTracks5 = df.query('nodes == 32 & vars == 5').sort_values(by = ['tracks'])
    nTracks50 = df.query('nodes == 32 & vars == 50').sort_values(by = ['tracks'])

    plt.plot(nTracks5['tracks'], nTracks5['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 5')
    plt.plot(nTracks20['tracks'], nTracks20['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 20')
    plt.plot(nTracks50['tracks'], nTracks50['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 50')
    plt.xlabel('nTracks')
    plt.ylabel('Time per event [ms]')
    plt.text(75, 0.2, "nodes = 32")
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/time_tracks32.pdf')
    plt.clf()

    plt.plot(nTracks5['tracks'], nTracks5['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 5')
    plt.plot(nTracks20['tracks'], nTracks20['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 20')
    plt.plot(nTracks50['tracks'], nTracks50['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 50')
    plt.xlabel('nTracks')
    plt.ylabel('Memory per 10k events [MB]')
    plt.text(20, 6.4, "nodes = 32")
    plt.ylim(5.2, 6.7)
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/memory_tracks32.pdf')
    plt.clf()

    #

    # nTracks for fixed nodes = 64, vars = 5, 20, 50
    nTracks20 = df.query('nodes == 64 & vars == 20').sort_values(by = ['tracks'])
    nTracks5 = df.query('nodes == 64 & vars == 5').sort_values(by = ['tracks'])
    nTracks50 = df.query('nodes == 64 & vars == 50').sort_values(by = ['tracks'])

    plt.plot(nTracks5['tracks'], nTracks5['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 5')
    plt.plot(nTracks20['tracks'], nTracks20['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 20')
    plt.plot(nTracks50['tracks'], nTracks50['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 50')
    plt.xlabel('nTracks')
    plt.ylabel('Time per event [ms]')
    plt.text(75, 0.5, "nodes = 64")
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/time_tracks64.pdf')
    plt.clf()

    plt.plot(nTracks5['tracks'], nTracks5['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 5')
    plt.plot(nTracks20['tracks'], nTracks20['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 20')
    plt.plot(nTracks50['tracks'], nTracks50['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 50')
    plt.xlabel('nTracks')
    plt.ylabel('Memory per 10k events [MB]')
    plt.text(20, 18.1, "nodes = 64")
    plt.ylim(16.8, 18.5)
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/memory_tracks64.pdf')
    plt.clf()

    # nTracks for fixed nodes = 128, vars = 5, 20, 50
    nTracks20 = df.query('nodes == 128 & vars == 20').sort_values(by = ['tracks'])
    nTracks5 = df.query('nodes == 128 & vars == 5').sort_values(by = ['tracks'])
    nTracks50 = df.query('nodes == 128 & vars == 50').sort_values(by = ['tracks'])

    plt.plot(nTracks5['tracks'], nTracks5['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 5')
    plt.plot(nTracks20['tracks'], nTracks20['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 20')
    plt.plot(nTracks50['tracks'], nTracks50['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 50')
    plt.xlabel('nTracks')
    plt.ylabel('Time per event [ms]')
    plt.text(75, 1.5, "nodes = 128")
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/time_tracks128.pdf')
    plt.clf()

    plt.plot(nTracks5['tracks'], nTracks5['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 5')
    plt.plot(nTracks20['tracks'], nTracks20['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 20')
    plt.plot(nTracks50['tracks'], nTracks50['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 50')
    plt.xlabel('nTracks')
    plt.ylabel('Memory per 10k events [MB]')
    plt.text(20, 65.0, "nodes = 128")
    plt.ylim(62.25, 65.5)
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/memory_tracks128.pdf')
    plt.clf()

    #

    # nVars for fixed nodes = 32, tracks = 20, 50, 100
    nVars20 = df.query('nodes == 32 & tracks == 20').sort_values(by = ['vars'])
    nVars50 = df.query('nodes == 32 & tracks == 50').sort_values(by = ['vars'])
    nVars100 = df.query('nodes == 32 & tracks == 100').sort_values(by = ['vars'])

    plt.plot(nVars20['vars'], nVars20['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 20')
    plt.plot(nVars50['vars'], nVars50['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 50')
    plt.plot(nVars100['vars'], nVars100['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 100')
    plt.xlabel('nVars')
    plt.ylabel('Time per event [ms]')
    plt.ylim(0.0, 1.0)
    plt.text(30, 0.85, "nodes = 32")
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/time_vars32.pdf')
    plt.clf()

    plt.plot(nVars20['vars'], nVars20['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 20')
    plt.plot(nVars50['vars'], nVars50['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks 5020')
    plt.plot(nVars100['vars'], nVars100['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 100')
    plt.xlabel('nVars')
    plt.ylabel('Memory per 10k events [MB]')
    plt.text(35, 5.65, "nodes = 32")
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/memory_vars32.pdf')
    plt.clf()

    #

    # nVars for fixed nodes = 64, tracks = 20, 50, 100
    nVars20 = df.query('nodes == 64 & tracks == 20').sort_values(by = ['vars'])
    nVars50 = df.query('nodes == 64 & tracks == 50').sort_values(by = ['vars'])
    nVars100 = df.query('nodes == 64 & tracks == 100').sort_values(by = ['vars'])

    plt.plot(nVars20['vars'], nVars20['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 20')
    plt.plot(nVars50['vars'], nVars50['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 50')
    plt.plot(nVars100['vars'], nVars100['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 100')
    plt.xlabel('nVars')
    plt.ylabel('Time per event [ms]')
    plt.ylim(0.25, 2.6)
    plt.text(10, 1.8, "nodes = 64")
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/time_vars64.pdf')
    plt.clf()

    plt.plot(nVars20['vars'], nVars20['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 20')
    plt.plot(nVars50['vars'], nVars50['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 50')
    plt.plot(nVars100['vars'], nVars100['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 100')
    plt.xlabel('nVars')
    plt.ylabel('Memory per 10k events [MB]')
    plt.text(35, 17.1, "nodes = 64")
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/memory_vars64.pdf')
    plt.clf()

    # nVars for fixed nodes = 128, tracks = 20, 50, 100
    nVars20 = df.query('nodes == 128 & tracks == 20').sort_values(by = ['vars'])
    nVars50 = df.query('nodes == 128 & tracks == 50').sort_values(by = ['vars'])
    nVars100 = df.query('nodes == 128 & tracks == 100').sort_values(by = ['vars'])

    plt.plot(nVars20['vars'], nVars20['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 20')
    plt.plot(nVars50['vars'], nVars50['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 50')
    plt.plot(nVars100['vars'], nVars100['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 100')
    plt.xlabel('nVars')
    plt.ylabel('Time per event [ms]')
    plt.ylim(0.8, 8.0)
    plt.text(10, 6.5, "nodes = 128")
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/time_vars128.pdf')
    plt.clf()

    plt.plot(nVars20['vars'], nVars20['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 20')
    plt.plot(nVars50['vars'], nVars50['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 50')
    plt.plot(nVars100['vars'], nVars100['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 100')
    plt.xlabel('nVars')
    plt.ylabel('Memory per 10k events [MB]')
    plt.text(35, 63, "nodes = 128")
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/memory_vars128.pdf')
    plt.clf()

    #

    # nVars for fixed nodes = 32, tracks = 20, 50, 100
    nNodes20 = df.query('vars == 20 & tracks == 20').sort_values(by = ['nodes'])
    nNodes50 = df.query('vars == 20 & tracks == 50').sort_values(by = ['nodes'])
    nNodes100 = df.query('vars == 20 & tracks == 100').sort_values(by = ['nodes'])

    plt.plot(nNodes20['nodes'], nNodes20['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 20')
    plt.plot(nNodes50['nodes'], nNodes50['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 50')
    plt.plot(nNodes100['nodes'], nNodes100['time'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 100')
    plt.xlabel('nNodes')
    plt.ylabel('Time per event [ms]')
    plt.text(20, 2.5, "vars = 20")
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/time_nodes20.pdf')
    plt.clf()

    plt.plot(nNodes20['nodes'], nNodes20['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 20')
    plt.plot(nNodes50['nodes'], nNodes50['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 50')
    plt.plot(nNodes100['nodes'], nNodes100['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'tracks = 100')
    plt.xlabel('nNodes')
    plt.ylabel('Memory per 10k events [MB]')
    plt.text(20, 30, "vars = 20")
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsPlots/memory_nodes20.pdf')
    plt.clf()

def plotCatBenchmarks():

    time = pickle.load( open('lwtnnTimingCat.pkl', 'r') )
    memory = pickle.load( open('lwtnnMemoryUsageCat.pkl', 'r') )

    rows_list = []

    for nodes in nodesList:
        for vars in varsList:
            for nCat in nCategoriesList:
                d = {'nodes' : nodes, 'vars' : vars, 'nCat' : nCat, 'time' : time[str(nodes) + '-' + str(vars) + '-' + str(nCat)] * 1E3, 'memory' : memory[str(nodes) + '-' + str(vars) + '-' + str(nCat)]}
                rows_list.append(d)

    df = pd.DataFrame(rows_list)

    # nodes for fixed nCat = 4, vars = 5, 20, 50
    nNodes20 = df.query('nCat == 4 & vars == 20').sort_values(by = ['nodes'])
    nNodes5 = df.query('nCat == 4 & vars == 5').sort_values(by = ['nodes'])
    nNodes50 = df.query('nCat == 4 & vars == 50').sort_values(by = ['nodes'])

    plt.plot(nNodes5['nodes'], nNodes5['time'] * 100, marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 5')
    plt.plot(nNodes20['nodes'], nNodes20['time'] * 100, marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 20')
    plt.plot(nNodes50['nodes'], nNodes50['time'] * 100, marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 50')
    plt.xlabel('nNodes')
    plt.ylabel('Time per 100 tracks [ms]')
    plt.text(100, 4.0, "nCat = 4")
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsCatPlots/time_nodes4.pdf')
    plt.clf()

    plt.plot(nNodes5['nodes'], nNodes5['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 5')
    plt.plot(nNodes20['nodes'], nNodes20['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 20')
    plt.plot(nNodes50['nodes'], nNodes50['memory'], marker = '+', markersize = 8.0, markeredgewidth = 1.0, linewidth = 1.0, label = 'vars = 50')
    plt.xlabel('nTracks')
    plt.ylabel('Memory per 10k tracks [MB]')
    plt.text(20, 6.4, "nCat = 4")
    # plt.ylim(5.2, 6.7)
    plt.legend(loc = 0)
    plt.savefig('lwtnnTestsCatPlots/memory_nodes4.pdf')
    plt.clf()

if __name__ == '__main__':
    plotCatBenchmarks()
